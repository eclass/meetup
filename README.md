# eClass Meetup

Nos juntamos el primer jueves de cada mes para conversar y compartir.

Temas de desarrollo, diseño o iniciativas desde el area de desarrollo y producto de 
eClass.

En cada [issue](https://gitlab.com/eclass/meetup/issues) dejaremos las presentaciones de cada mes.

![hi5](https://cdn.dribbble.com/users/1419018/screenshots/3635374/hithere_0706-02.png)